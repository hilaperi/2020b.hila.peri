package acs;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import acs.data.UserRole;
import acs.rest.boundaries.user.NewUserDetailsBoundary;
import acs.rest.boundaries.user.UserBoundary;
import acs.rest.boundaries.user.UserIdBoundary;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserTests {

	private RestTemplate restTemplate;
	private String url;
	private int port;

	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}

	@PostConstruct
	public void init() {
		this.restTemplate = new RestTemplate();
		this.url = "http://localhost:" + this.port + "/acs";
	}

	@AfterEach
	public void teardown() {
		this.restTemplate.delete(this.url + "/admin/users/{adminDomain}/{adminEmail}", "???", "??");
	}

	@Test
	public void testContext() {

	}

	@Test
	public void test_Post_Create_New_User_Then_The_Database_Contains_Same_User_With_Same_Id() throws Exception {
		
		UserIdBoundary postedUserId = this.restTemplate.postForObject(this.url + "/users",
				new NewUserDetailsBoundary("hagit@gmail.com", UserRole.PLAYER, "hagit", "YY"), UserBoundary.class)
				.getUserId();


		String userDomain = postedUserId.getDomain();
		String userEmail = postedUserId.getEmail();

		UserIdBoundary actualUserId = this.restTemplate.getForObject(this.url + "/users/login/{userDomain}/{userEmail}",
				UserBoundary.class, userDomain, userEmail).getUserId();

		assertThat(actualUserId).extracting("domain", "email").usingRecursiveFieldByFieldElementComparator()
				.containsExactly(userDomain, userEmail);



	}

	@Test
	public void test_Post_New_User_Then_The_Database_Contains_A_Single_User() throws Exception {
		// GIVEN the server is up
		// do nothing

		// WHEN I POST new user
		this.restTemplate.postForObject(this.url + "/users",
				new NewUserDetailsBoundary("hila@gmail.com", UserRole.PLAYER, "hila", "YYY"), UserBoundary.class);

		// THEN the database contains a single message
		assertThat(this.restTemplate.getForObject(this.url + "/admin/users/{adminDomain}/{adminEmail}",
				UserBoundary[].class, "???", "??")).hasSize(1);
	}

	@Test
	public void test_Post_New_User_Then_The_Database_Contains_User_With_The_Same_User_Attribute_Role()
			throws Exception {
		// GIVEN the server is up
		// do nothing

		// WHEN I POST new user with user role attribute: "PLAYER"

		this.restTemplate.postForObject(this.url + "/users",
				new NewUserDetailsBoundary("paz@gmail.com", UserRole.PLAYER, "???", "???"), UserBoundary.class);

		// THEN the database contains a user with user role attribute "PLAYER"
		UserBoundary[] allUsers = this.restTemplate.getForObject(this.url + "/admin/users/{adminDomain}/{adminEmail}",
				UserBoundary[].class, "???", "??");
		boolean containsPlayerRole = false;
		for (UserBoundary m : allUsers) {
			if (m.getRole().equals(UserRole.PLAYER)) {
				containsPlayerRole = true;
			}
		}

		if (!containsPlayerRole) {
			throw new Exception("failed to locate user with proper attribute role");
		}

	}

	@Test
	public void test_Post_New_User_Then_The_Database_Contains_User_With_The_Same_User_Attribute_UserName()
			throws Exception {
		// GIVEN the server is up
		// do nothing

		// WHEN I POST new user with userName attribute: "anna"

		this.restTemplate.postForObject(this.url + "/users",
				new NewUserDetailsBoundary("anna@gmail.com", UserRole.PLAYER, "anna", "???"), UserBoundary.class);

		// THEN the database contains a user with userName attribute "anna"
		UserBoundary[] allUsers = this.restTemplate.getForObject(this.url + "/admin/users/{adminDomain}/{adminEmail}",
				UserBoundary[].class, "???", "??");
		boolean containsUserNameAnna = false;
		for (UserBoundary m : allUsers) {
			if (m.getUsername().equals("anna")) {
				containsUserNameAnna = true;
			}
		}

		if (!containsUserNameAnna) {
			throw new Exception("failed to locate user with proper attribute user name");
		}

	}

	@Test
	public void test_Post_New_User_Then_The_Database_Contains_User_With_The_Same_User_Attribute_Avatar()
			throws Exception {

		this.restTemplate.postForObject(this.url + "/users",
				new NewUserDetailsBoundary("paz@gmail.com", UserRole.PLAYER, "???", "YY"), UserBoundary.class);


		UserBoundary[] allUsers = this.restTemplate.getForObject(this.url + "/admin/users/{adminDomain}/{adminEmail}",
				UserBoundary[].class, "???", "??");
		boolean containsAvatarAttribute = false;
		for (UserBoundary m : allUsers) {
			if (m.getAvatar().equals("YY")) {
				containsAvatarAttribute = true;
			}
		}

		if (!containsAvatarAttribute) {
			throw new Exception("failed to locate user with proper attribute avatar");
		}

	}

	@Test
	public void test_Put_Update_User_Attribute_Role_To_MANAGER_Then_Role_Is_Updated_In_The_DataBase() throws Exception {
		
		UserBoundary boundaryOnServer = this.restTemplate.postForObject(this.url + "/users",
				new NewUserDetailsBoundary("hila@gmail.com", UserRole.PLAYER, "hila", "YYYY"), UserBoundary.class);

		UserIdBoundary postedUserId = boundaryOnServer.getUserId();
		String userDomain = postedUserId.getDomain();
		String userEmail = postedUserId.getEmail();


		UserBoundary update = new UserBoundary();
		update.setRole(UserRole.MANAGER);
		this.restTemplate.put(this.url + "/users/{userDomain}/{userEmail}", update, userDomain, userEmail);

		assertThat(this.restTemplate.getForObject(this.url + "/users/login/{userDomain}/{userEmail}",
				UserBoundary.class, userDomain, userEmail)).extracting("userId", "role")
						.usingRecursiveFieldByFieldElementComparator()
						.containsExactly(boundaryOnServer.getUserId(), update.getRole());
	}

	@Test
	public void test_Put_Update_User_Attribute_Avatar_Then_Avatar_Is_Updated_In_The_DataBase() throws Exception {


		UserBoundary boundaryOnServer = this.restTemplate.postForObject(this.url + "/users",
				new NewUserDetailsBoundary("paz@gmail.com", UserRole.PLAYER, "paz", "YYY"), UserBoundary.class);

		UserIdBoundary postedUserId = boundaryOnServer.getUserId();
		String userDomain = postedUserId.getDomain();
		String userEmail = postedUserId.getEmail();

	
		UserBoundary update = new UserBoundary();
		update.setAvatar("Y");
		this.restTemplate.put(this.url + "/users/{userDomain}/{userEmail}", update, userDomain, userEmail);


		assertThat(this.restTemplate.getForObject(this.url + "/users/login/{userDomain}/{userEmail}",
				UserBoundary.class, userDomain, userEmail)).extracting("userId", "avatar")
						.usingRecursiveFieldByFieldElementComparator()
						.containsExactly(boundaryOnServer.getUserId(), update.getAvatar());
	}

	@Test
	public void test_Init_Server_With_3_Users_When_We_Get_All_Users_We_Receive_The_Same_Users_Initialized()
			throws Exception {


		List<UserBoundary> allUsersInDb = IntStream.range(1, 4).mapToObj(i -> ("email" + i + "@gmail.com"))
				.map(email -> new NewUserDetailsBoundary(email, UserRole.PLAYER, "hagit", "YY"))
				.map(boundary -> this.restTemplate.postForObject(this.url + "/users", boundary, UserBoundary.class))
				.collect(Collectors.toList());
		for (UserBoundary userBoundary : allUsersInDb) {
			System.out.println("before" + userBoundary);
		}

		
		UserBoundary[] allUsers = this.restTemplate.getForObject(this.url + "/admin/users/{adminDomain}/{adminEmail}",
				UserBoundary[].class, "???", "??");

		for (UserBoundary userBoundary : allUsers) {
			System.out.println("after" + userBoundary);
		}

		assertThat(allUsers).hasSize(allUsersInDb.size()).usingRecursiveFieldByFieldElementComparator()
				.containsExactlyInAnyOrderElementsOf(allUsersInDb);
	}

	@Test
	public void test_The_Database_Is_Empty_By_Default() throws Exception {

		UserBoundary[] actual = this.restTemplate.getForObject(this.url + "/admin/users/{adminDomain}/{adminEmail}",
				UserBoundary[].class, "???", "??");


		assertThat(actual).isEmpty();
	}

	@Test
	public void test_Init_Server_With_3_Users_When_We_Delete_all_Users_And_Than_Get_All_Users_We_Receive_empty_array()
			throws Exception {

		this.restTemplate.delete(this.url + "/admin/users/{adminDomain}/{adminEmail}", "???", "??");

		UserBoundary[] actual = this.restTemplate.getForObject(this.url + "/admin/users/{adminDomain}/{adminEmail}",
				UserBoundary[].class, "???", "??");
		assertThat(actual).isEmpty();
	}

}
