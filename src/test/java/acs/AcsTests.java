package acs;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import acs.data.UserRole;
import acs.rest.boundaries.user.NewUserDetailsBoundary;
import acs.rest.boundaries.user.UserBoundary;
import acs.rest.boundaries.user.UserIdBoundary;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AcsTests {
	private int port;
	private String url;
	private RestTemplate restTemplate;
	
	
	
	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}
	
	@PostConstruct
	public void init() {
		this.url = "http://localhost:" + this.port + "/acs";
		this.restTemplate = new RestTemplate();
	}
	
	@BeforeEach
	public void setup() throws Exception{
		// do nothing
//		UserBoundary user = new UserBoundary(new UserIdBoundary("hagit","hagit@comm"), UserRole.ADMIN, "hagit", ":0");
//		
//		UserBoundary admin = this.restTemplate.postForObject(this.url + "/users", user, UserBoundary.class);
//		
//		if(!(admin.getUserId().getEmail().equals(user.getUserId().getEmail())&& admin.getUserId().getDomain().equals(user.getUserId().getDomain())&& admin.getRole().equals(UserRole.ADMIN))) {
//			throw new Exception("expected user boundary but it was wrong");
//		}
	}
	
	
	
	@AfterEach
	public void tearDown() throws Exception {
		
		
		
		NewUserDetailsBoundary user = new NewUserDetailsBoundary( "hagit12@com", UserRole.ADMIN, "hagit", ":-)");
		UserBoundary admin = this.restTemplate.postForObject(this.url + "/users", user, UserBoundary.class);
		
		if(!(admin.getUserId().getEmail().equals(user.getEmail())&&admin.getRole().equals(UserRole.ADMIN)&& admin.getAvatar().equals(user.getAvatar()))) {
			throw new Exception("expected user boundary but it was wrong");
		}
		
		this.restTemplate.delete(this.url + "/admin/users/hagit/hagit@com");
		
		
	}
	
	
	
	@Test
	public void testContext() {
		
	}
	
	//users Tests
	@Test
	public void testCreateNewUser() throws Exception{
		
		//GIVEN the server is up 
		
		//WHEN I  POST /acs/users
		NewUserDetailsBoundary user = new NewUserDetailsBoundary( "hagit@com", UserRole.PLAYER, "hagit", ":-)");
		
		UserBoundary output = this.restTemplate.postForObject(this.url+ "/users", user, UserBoundary.class);
		
		
		//THEN the server respons with status 2xx
		//AND the responds with the user details
		
		if(!(output.getUserId().getEmail().equals(user.getEmail())&&output.getRole().equals(UserRole.PLAYER)&& output.getAvatar().equals(user.getAvatar()))) {
			throw new Exception("expected user boundary but it was wrong");
		
			
		}
			
	}
	
	
//	@Test
//	public void testUpdateUser() throws Exception{
//		//GIVEN the server is up
//		
//		
//		
//		
//	}
	

	
	
	
	
	
	//=====================================================================
	
	
	
	//element Tests 
	
	
	
	
	//======================================================================
	
	//action Tests
	
	
	
	
	
}





