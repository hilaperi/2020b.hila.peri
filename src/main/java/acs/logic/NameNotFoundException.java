package acs.logic;

public class NameNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -6508025474230044999L;

	public NameNotFoundException() {
		super();
	}

	public NameNotFoundException(String message) {
		super(message);
	}
	
	public NameNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public NameNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
