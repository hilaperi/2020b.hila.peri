package acs.logic;

public class ElementNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 795109905661252424L;

	public ElementNotFoundException() {
		super();
	}

	public ElementNotFoundException(String message) {
		super(message);
	}
	
	public ElementNotFoundException(Throwable cause) {
		super(cause);
	}
	
	public ElementNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
