package acs.logic.db;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import acs.aop.LogThisOperation;
import acs.aop.PerformanceElapsedMonitor;
import acs.aop.PerformanceUnit;
import acs.dal.UserDao;
import acs.data.Converter;
import acs.data.UserEntity;
import acs.data.UserIdEntity;
import acs.data.UserRole;
import acs.logic.ExtendedUserService;
import acs.logic.ObjectNotFoundException;
//import acs.logic.ServiceTools;
import acs.logic.UserNotFoundException;
import acs.rest.boundaries.user.UserBoundary;

@Service
public class UserServiceDb implements ExtendedUserService {

	private String projectName;
	private UserDao userDao;
	private Converter converter;

	@Autowired
	public UserServiceDb(UserDao userDao, Converter converter) {
		this.converter = converter;
		this.userDao = userDao;
	}

	// injection of project name from the spring boot configuration
	@Value("${spring.application.name: generic}")
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@PostConstruct
	public void init() {
		// initialize object after injection
		System.err.println("project name: " + this.projectName);
	}

	@Override
	@Transactional
	@LogThisOperation
	@PerformanceElapsedMonitor(units = PerformanceUnit.ns)
	public UserBoundary createUser(UserBoundary user) {
		if (user.getAvatar() == null || user.getAvatar().trim().isEmpty()) {
			throw new RuntimeException("the Avatar of user cannot be null or empty string");
		}
		if (user.getUsername() == null || user.getUsername().trim().isEmpty()) {
			throw new RuntimeException("the username cannot be null or empty string");
		}

		user.getUserId().setDomain(this.projectName);
		UserEntity entity = this.converter.toEntity(user);
		return this.converter.fromEntity(this.userDao.save(entity));
	}

	@Override
	@Transactional(readOnly = true)
	public UserBoundary login(String userDomain, String userEmail) {

		UserIdEntity userId = new UserIdEntity(userDomain, userEmail);
		UserEntity existing = this.userDao.findById(userId).orElseThrow(() -> new UserNotFoundException(
				"could not find user by UserDomain: " + userDomain + "or userEmail: " + userEmail));

		return this.converter.fromEntity(existing);

	}

	@Override
	@Transactional
	public UserBoundary updateUser(String userDomain, String userEmail, UserBoundary update) {

		UserIdEntity userId = new UserIdEntity(userDomain, userEmail);
		UserEntity existing = this.userDao.findById(userId).orElseThrow(() -> new UserNotFoundException(
				"could not find user by UserDomain: " + userDomain + "or userEmail: " + userEmail));

		if (update.getRole() != null) {
			existing.setRole(update.getRole());
		}
		if (update.getUsername() != null) {
			existing.setUsername(update.getUsername());
		}
		if (update.getAvatar() != null) {
			existing.setAvatar(update.getAvatar());
		}

		if (existing.getUsername() == null || existing.getUsername().trim().isEmpty()) {
			throw new RuntimeException("the username cannot be null or empty string");
		}

		return this.converter.fromEntity(this.userDao.save(existing));

	}

	@Override
	@Transactional(readOnly = true)
	public List<UserBoundary> getAllUsers(String adminDomain, String adminEmail) {

		UserEntity userEntity = this.userDao.findById(new UserIdEntity(adminDomain, adminEmail))
				.orElseThrow(() -> new ObjectNotFoundException(
						"could not find user by userDomain: " + adminDomain + " and userEmail: " + adminEmail));

		if (userEntity.getRole() != UserRole.ADMIN) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only Admin can export all users!");
		}

		return StreamSupport.stream(this.userDao.findAll().spliterator(), false).map(this.converter::fromEntity)
				.collect(Collectors.toList());

	}

	@Override
	@Transactional
	public void deleteAllUsers(String adminDomain, String adminEmail) {

		UserIdEntity uIB = new UserIdEntity(adminDomain, adminEmail);

		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by adminDomain:" + adminDomain + "or adminEmail:" + adminEmail));

		if (!existingUser.getRole().equals(UserRole.ADMIN))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only Admin can delete users!");
		this.userDao.deleteAll();

	}

	@Override
	public List<UserBoundary> getAllUsers(String adminDomain, String adminEmail, int size, int page) {
//		ServiceTools.stringValidation(adminDomain, adminEmail);
//		
//		ServiceTools.validatePaging(size, page);
		UserEntity userEntity = this.userDao.findById(new UserIdEntity(adminDomain, adminEmail))
				.orElseThrow(() -> new ObjectNotFoundException(
						"could not find user by userDomain: " + adminDomain + " and userEmail: " + adminEmail));

		if (userEntity.getRole() != UserRole.ADMIN) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only Admin can export all users!");
		}

		return this.userDao.findAll(PageRequest.of(page, size, Direction.DESC, "userId"))// Page<UserEntity>
				.getContent()// List<UserEntity>
				.stream()// Stream<UserEntity>
				.map(this.converter::fromEntity)// Stream<UserEntity>
				.collect(Collectors.toList()); // List<UserEntity>
	}

}
