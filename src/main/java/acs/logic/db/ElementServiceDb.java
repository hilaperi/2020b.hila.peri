package acs.logic.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import acs.aop.LogThisOperation;
import acs.aop.PerformanceElapsedMonitor;
import acs.aop.PerformanceUnit;
import acs.dal.ElementDao;
import acs.dal.UserDao;
import acs.data.Converter;
import acs.data.ElementEntity;
import acs.data.ElementIdEntity;
import acs.data.UserEntity;
import acs.data.UserIdEntity;
import acs.data.UserRole;
import acs.logic.ElementNotFoundException;
import acs.logic.ExtendedElementService;
import acs.logic.ObjectNotFoundException;
import acs.logic.UserNotFoundException;
import acs.rest.boundaries.element.ElementBoundary;
import acs.rest.boundaries.element.ElementIdBoundary;

@Service
public class ElementServiceDb implements ExtendedElementService {
	private ElementDao elementDao;
	private Converter converter;
	private String projectName;
	private UserDao userDao;

	@Autowired
	public ElementServiceDb(ElementDao elementDao, Converter converter, UserDao userDao) {
		this.elementDao = elementDao;
		this.converter = converter;
		this.userDao = userDao;
	}

	// injection of project name from the spring boot configuration
	@Value("${spring.application.name: generic}")
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@PostConstruct
	public void init() {
		// since this class is a singleton, we generate a thread safe collection
		System.err.println("project name: " + this.projectName);
	}

	@Override
	@Transactional
	@LogThisOperation
	@PerformanceElapsedMonitor(units = PerformanceUnit.ns)
	public ElementBoundary create(String managerDomain, String managerEmail, ElementBoundary element) {
		if (element.getType() == null || element.getType().trim().isEmpty()) {
			throw new RuntimeException("Type of Element cannot be null");
		}

		UserIdEntity uib = new UserIdEntity(managerDomain, managerEmail);
		UserEntity existing = this.userDao.findById(uib).orElseThrow(() -> new UserNotFoundException(
				"could not find object by UserDomain:" + managerDomain + "or userEmail:" + managerEmail));

		if (!existing.getRole().equals(UserRole.MANAGER))
			throw new ObjectNotFoundException("only manager can create element!");

		element.setElementId(new ElementIdBoundary(projectName, UUID.randomUUID().toString()));
		ElementEntity entity = this.converter.toEntity(element);
		entity.setCreatedBy(managerDomain + " " + managerEmail);
		entity.setCreatedTimestamp(new Date());
		return this.converter.fromEntity(this.elementDao.save(entity));
	}

	@Override
	@Transactional
	@PerformanceElapsedMonitor(units = PerformanceUnit.ns)
	public ElementBoundary update(String managerDomain, String managerEmail, String elementDomain, String elementId,
			ElementBoundary update) {

		UserIdEntity uIB = new UserIdEntity(managerDomain, managerEmail);

		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by UserDomain:" + managerDomain + "or userEmail:" + managerEmail));

		if (!existingUser.getRole().equals(UserRole.MANAGER))
			throw new ObjectNotFoundException("You are not manager! Can't update an element");

		ElementEntity existing = this.converter
				.toEntity(this.getSpecificElement(managerDomain, managerEmail, elementDomain, elementId));

		if (existing == null)
			throw new ElementNotFoundException(
					"could not find element by id and domain: " + elementDomain + " " + elementId);
		else {
			if (update.getActive() != null)
				existing.setActive(update.getActive());
			if (update.getName() != null)
				existing.setName(update.getName());
			if (update.getLocation() != null)
				existing.setLocation(
						this.converter.toEntity(update.getLocation()));
			
			if (update.getType() != null)
				existing.setType(update.getType());
			if (update.getElementAttributes() != null) {
				existing.setElementAttributes(update.getElementAttributes());
			}
			
			if(existing.getParent() != null) {
				existing.getParent().getCreatedTimestamp();
			}
			
			existing.getChildren().size();

			if (existing.getName() == null || existing.getName().trim().isEmpty()) {
				throw new RuntimeException("the name cannot be null or empty string");
			}

			
			this.elementDao.save(existing);

			return this.converter.fromEntity(existing);
		}

	}

	@Override
	@Transactional(readOnly = true)
	@PerformanceElapsedMonitor(units = PerformanceUnit.ns)
	public List<ElementBoundary> getAll(String userDomain, String userEmail) {
		
		UserEntity userEntity = this.userDao.findById(new UserIdEntity(userDomain, userEmail))
				.orElseThrow(() -> new UserNotFoundException(
						"could not find user by userDomain: " + userDomain + "and userEmail: " + userEmail));
		if (userEntity.getRole() == UserRole.ADMIN)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Admin User Can't Get Elements");
		
		return StreamSupport.stream(this.elementDao.findAll().spliterator(), false).map(this.converter::fromEntity)
				.collect(Collectors.toList());

	}

	@Override
	@Transactional(readOnly = true)
	@PerformanceElapsedMonitor(units = PerformanceUnit.ns)
	public ElementBoundary getSpecificElement(String userDomain, String userEmail, String elementDomain,
			String elementId) {

		UserEntity userEntity = this.userDao.findById(new UserIdEntity(userDomain, userEmail))
				.orElseThrow(() -> new UserNotFoundException(
						"could not find user by userDomain: " + userDomain + "and userEmail: " + userEmail));
		if (userEntity.getRole() == UserRole.ADMIN)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Admin User Can't Get Specific Element");

		ElementEntity existing = this.elementDao.findById(new ElementIdEntity(elementDomain, elementId))
				.orElseThrow(() -> new ElementNotFoundException(
						"could not find element by elementDomain: " + elementDomain + "or elementId: " + elementId));

		if (userEntity.getRole() == UserRole.PLAYER && !existing.getActive())
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Player User Can't Get Specific Inactive Element");

		return this.converter.fromEntity(existing);
	}

	@Override
	@Transactional
	@PerformanceElapsedMonitor(units = PerformanceUnit.ns)
	public void deleteAllElements(String adminDomain, String adminEmail) {
		UserIdEntity uIB = new UserIdEntity(adminDomain, adminEmail);

		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by adminDomain:" + adminDomain + "or adminEmail:" + adminEmail));

		if (!existingUser.getRole().equals(UserRole.ADMIN))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only Admin can delete elements!");

		if (adminDomain != null && !adminDomain.trim().isEmpty() && adminEmail != null
				&& !adminEmail.trim().isEmpty()) {
			this.elementDao.deleteAll();
		} else {
			throw new RuntimeException("Admin Domain and Admin Email must not be empty or null");
		}

	}

	@Override
	@Transactional
	@PerformanceElapsedMonitor(units = PerformanceUnit.ns)
	public void bindExistingElementToAnExistingChildElement(String managerDomain, String managerEmail,
			ElementIdBoundary elementParentId, ElementIdBoundary elementChildId) {

		UserEntity userEntity = this.userDao.findById(new UserIdEntity(managerDomain, managerEmail))
				.orElseThrow(() -> new UserNotFoundException(
						"could not find user by userDomain: " + managerDomain + "and userEmail: " + managerEmail));
		if (userEntity.getRole() != UserRole.MANAGER)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "only MANAGER can bind elements");

		if (elementChildId == null) {
			throw new ElementNotFoundException("no element in the database");
		}

		ElementEntity parent = this.elementDao.findById(converter.toEntity(elementParentId)).orElseThrow(
				() -> new ElementNotFoundException("could not find parentElement by id: " + elementParentId));

		ElementEntity child = this.elementDao.findById(converter.toEntity(elementChildId)).orElseThrow(
				() -> new ElementNotFoundException("could not find childElement by id: " + elementChildId));

		parent.addChild(child);
		this.elementDao.save(parent);
	}

	@Override
	@Transactional(readOnly = true)
	@PerformanceElapsedMonitor
	public Set<ElementBoundary> getAllChildrenOfAnExsitingElement(String userDomain, String userEmail,
			String elementParentDomain, String elementParentId, int size, int page) {

		return this.elementDao
				.findAllByParent_ElementId_IdAndParent_ElementId_Domain(elementParentId, elementParentDomain,
						PageRequest.of(page, size, Direction.DESC, "elementId"))
				.stream().map(this.converter::fromEntity).collect(Collectors.toSet());
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<ElementBoundary> getAnArrayWithElementParent(String userDomain, String userEmail, String elementChildDomain,
			String elementChildId, int size, int page) {
		
		UserEntity uE = this.userDao.findById(new UserIdEntity(userDomain, userEmail))
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
						"could not find user by userDomain: " + userDomain + " and userEmail: " + userEmail));
		
		if (uE.getRole().equals(UserRole.ADMIN))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Admin can't get element children \n");

		
		ElementEntity child = this.elementDao.findById(new ElementIdEntity(elementChildDomain, elementChildId))
				.orElseThrow(
						() -> new ElementNotFoundException("could not find child element by id:" + elementChildId));

		ElementEntity parent = child.getParent();
		
		if(uE.getRole().equals(UserRole.PLAYER)) {
			if(!child.getActive() || !parent.getActive())
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "player can get only active elements\n");

		}
		
		Collection<ElementBoundary> eb = new HashSet<>();
		if(page > 1)
			return eb;
		if(parent != null && page == 0) {
			ElementBoundary ebBoundary = this.converter.fromEntity(parent);
			eb.add(ebBoundary);
		}
		return eb;
		
	}	
//		if (child.getParent() != null) {
//			return this.converter.fromEntity(child.getParent());
//		} else {
//			return null;
//		}


	@Override
	@Transactional(readOnly = true)
	@PerformanceElapsedMonitor
	public List<ElementBoundary> getElementsByName(String userDomain, String userEmail, String name, int size,
			int page) {
		UserEntity userEntity = this.userDao.findById(new UserIdEntity(userDomain, userEmail))
				.orElseThrow(() -> new UserNotFoundException(
						"could not find user by userDomain: " + userDomain + "and userEmail: " + userEmail));
		if (userEntity.getRole() == UserRole.ADMIN)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Admin User Can't Search Elements By Location");
		if (userEntity.getRole() == UserRole.PLAYER)
			return this.elementDao
					.findAllByNameAndActive(name, true, PageRequest.of(page, size, Direction.DESC, "elementId"))
					.stream().map(this.converter::fromEntity).collect(Collectors.toList());
		if (userEntity.getRole() == UserRole.MANAGER)
			return this.elementDao.findAllByName(name, PageRequest.of(page, size, Direction.DESC, "elementId")).stream()
					.map(this.converter::fromEntity).collect(Collectors.toList());
		return new ArrayList<>();
	}

	@Override
	@Transactional(readOnly = true)
	@PerformanceElapsedMonitor
	public List<ElementBoundary> getAll(String userDomain, String userEmail, int size, int page) {

		UserIdEntity uIB = new UserIdEntity(userDomain, userEmail);
		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by UserDomain:" + userDomain + "or userEmail:" + userEmail));

		// if user is MANAGER : findAll
		if (existingUser.getRole().equals(UserRole.MANAGER)) {
			return this.elementDao.findAll(PageRequest.of(page, size, Direction.DESC, "elementId")) // Page<ElementEntity>
					.getContent() // List<ElementEntity>
					.stream() // Stream<ElementEntity>
					.map(this.converter::fromEntity) // Stream<ElementBoundary>
					.collect(Collectors.toList()); // List<ElementBoundary>
		}

		// if user = PLAYER : findAllByActive
		else if (existingUser.getRole().equals(UserRole.PLAYER)) {
			return this.elementDao
					.findAllByActive(Boolean.TRUE, PageRequest.of(page, size, Direction.DESC, "elementId")) // Page<ElementEntity>
					.stream() // Stream<ElementEntity>
					.map(this.converter::fromEntity) // Stream<ElementBoundary>
					.collect(Collectors.toList()); // List<ElementBoundary>
		}

		return new ArrayList<>();
	}

	@Override
	@Transactional(readOnly = true)
	@PerformanceElapsedMonitor
	public List<ElementBoundary> getElementsByType(String userDomain, String userEmail, String type, int size,
			int page) {
		UserEntity userEntity = this.userDao.findById(new UserIdEntity(userDomain, userEmail))
				.orElseThrow(() -> new UserNotFoundException(
						"could not find user by userDomain: " + userDomain + " and userEmail: " + userEmail));

		if (userEntity.getRole() == UserRole.MANAGER) {
			return this.elementDao.findAllByType(type, PageRequest.of(page, size, Direction.ASC, "elementId")).stream()
					.map(this.converter::fromEntity).collect(Collectors.toList());
		}
		if (userEntity.getRole() == UserRole.PLAYER)
			return this.elementDao
					.findAllByTypeAndActive(type, true, PageRequest.of(page, size, Direction.ASC, "elementId")).stream()
					.map(this.converter::fromEntity).collect(Collectors.toList());

		if (userEntity.getRole() == UserRole.ADMIN) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Admin User Can't Search Elements By Type");

		}
		return new ArrayList<>();
	}

	@Override
	@Transactional(readOnly = true)
	@PerformanceElapsedMonitor
	public Collection<ElementBoundary> searchByLocation(String userDomain, String userEmail, double lat, double lng,
			double distance, int size, int page) {

		UserEntity userEntity = this.userDao.findById(new UserIdEntity(userDomain, userEmail))
				.orElseThrow(() -> new UserNotFoundException(
						"could not find user by userDomain: " + userDomain + "and userEmail: " + userEmail));

		if (userEntity.getRole() == UserRole.MANAGER)
			return this.elementDao
					.findAllByLocation_LatBetweenAndLocation_LngBetween(lat - distance, lat + distance, lng - distance,
							lng + distance, PageRequest.of(page, size, Direction.ASC, "elementId"))
					.stream().map(this.converter::fromEntity).collect(Collectors.toList());

		if (userEntity.getRole() == UserRole.PLAYER)
			return this.elementDao
					.findAllByLocation_LatBetweenAndLocation_LngBetweenAndActive(lat - distance, lat + distance,
							lng - distance, lng + distance, true,
							PageRequest.of(page, size, Direction.ASC, "elementId"))
					.stream().map(this.converter::fromEntity).collect(Collectors.toList());

		if (userEntity.getRole() == UserRole.ADMIN)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Admin User Can't Search Elements By Location");

		return new ArrayList<>();
	}

}
