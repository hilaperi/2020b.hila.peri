package acs.logic.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import acs.dal.ActionDao;
import acs.dal.ElementDao;
import acs.dal.UserDao;
import acs.data.ActionEntity;
import acs.data.Converter;
import acs.data.ElementEntity;
import acs.data.ElementIdEntity;
import acs.data.UserEntity;
import acs.data.UserIdEntity;
import acs.data.UserRole;
import acs.logic.ExtendedActionService;
import acs.logic.ExtendedElementService;
import acs.logic.ObjectNotFoundException;
import acs.logic.UserNotFoundException;
import acs.logic.operations.ClassOperations;
import acs.logic.operations.CollegeOperations;
import acs.logic.operations.UserOperations;
//import acs.logic.ServiceTools;
import acs.rest.boundaries.action.ActionBoundary;
import acs.rest.boundaries.action.ActionIdBoundary;
import acs.rest.boundaries.element.ClassAttributes;
import acs.rest.boundaries.element.CreatedByBoundary;
import acs.rest.boundaries.element.ElementBoundary;
import acs.rest.boundaries.element.ElementIdBoundary;
import acs.rest.boundaries.element.ElementLocationBoundary;
import acs.rest.boundaries.user.UserBoundary;

@Service
public class ActionServiceDb implements ExtendedActionService {

	private String projectName;
	private ActionDao actionDao;
	private Converter converter;
	private ElementDao elementDao;
	private UserDao userDao;
	private ExtendedElementService elementService;
	private static int count = 0;
	@Autowired
	public ActionServiceDb(ActionDao actionDao, Converter converter, ElementDao elementDao, UserDao userDao,ExtendedElementService elementService) {
		this.converter = converter;
		this.actionDao = actionDao;
		this.elementDao = elementDao;
		this.userDao = userDao;
		this.elementService = elementService;
	}

	
	// injection of project name from the spring boot configuration
	@Value("${spring.application.name: generic}")
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

		
	@Override
	@Transactional
	public Object invokeAction(ActionBoundary action) {
		String playerDomain = action.getInvokedBy().getUserId().getDomain();
		String playerEmail = action.getInvokedBy().getUserId().getEmail();
		UserIdEntity uIB = new UserIdEntity(playerDomain, playerEmail);

		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by playerDomain: " + playerDomain + " or playerEmail " + playerEmail));

		if (!existingUser.getRole().equals(UserRole.PLAYER))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only player can invoke actions!");
		
		
		if (action.getType() == null || action.getType().trim().isEmpty()) {
			throw new RuntimeException("Type of Action cannot be null");
		}
		if (action.getElement().getElementId() == null
				|| action.getElement().getElementId().getDomain().trim().isEmpty()
				|| action.getElement().getElementId().getId().trim().isEmpty()) {
			throw new RuntimeException("element id of action cannot be null");
		}

		if (action.getInvokedBy().getUserId() == null || action.getInvokedBy().getUserId().getDomain().trim().isEmpty()
				|| action.getInvokedBy().getUserId().getEmail().trim().isEmpty()) {
			throw new RuntimeException("details of user that invoked the action cannot be null");
		}

		ElementIdEntity elementIdOfAction = this.converter.toEntity(action.getElement().getElementId());
		ElementEntity element = this.elementDao.findById(elementIdOfAction)
				.orElseThrow(() -> new ObjectNotFoundException("could not find object by ElementDomain:"
						+ elementIdOfAction.getDomain() + " or ElementId:" + elementIdOfAction.getId()));
		
		if (element.getActive()) {
			action.setCreatedTimestamp(new Date());
			action.setActionId(new ActionIdBoundary(projectName, UUID.randomUUID().toString()));
			//user operations
			if(action.getType().equals("getAvailableClasses")) {
				ElementBoundary classElement = updateClassRoom(this.converter.fromEntity(element), false, this.converter.fromEntity(existingUser));
				this.actionDao.save(this.converter.toEntity(action));
				return classElement;
				
			}else if(action.getType().equals("reportEnterToClass")) {
				ElementBoundary classElement = enterClassOrLeaveClass(this.converter.fromEntity(element), this.converter.fromEntity(existingUser), true, action);
				this.actionDao.save(this.converter.toEntity(action));
				return classElement;
			}else if(action.getType().equals("reportLeavingClass")) {
				ElementBoundary classElement = enterClassOrLeaveClass(this.converter.fromEntity(element), this.converter.fromEntity(existingUser), false, action);
				this.actionDao.save(this.converter.toEntity(action));
				return classElement;
			}
			//class operations
			    else if(action.getType().equals("checkRoomNumber")) {
		
				
			}else if(action.getType().equals("checkNumberOfClasses")) {
				new CollegeOperations().checkNumberOfClasses(action);
		
			}else if(action.getType().equals("createClasses")) {
				
				if(count == 0) {
				ElementBoundary eleb = this.converter.fromEntity(element);
				this.createClassRooms(eleb, element.getActive());
				count++;
				}else {
					action.getActionAttributes().put("Error", "classes already created");
				}
			}
			
			ActionEntity entity = this.converter.toEntity(action);
			this.actionDao.save(entity);
			return action;
	}
		
		
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Admin User Can't Search Elements By Location");
	}


	@Override
	@Transactional(readOnly = true)
	public List<ActionBoundary> getAllActions(String adminDomain, String adminEmail) {
		UserIdEntity uIB = new UserIdEntity(adminDomain, adminEmail);

		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by adminDomain:" + adminDomain + "or adminEmail:" + adminEmail));

		if (!existingUser.getRole().equals(UserRole.ADMIN))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only Admin can export actions!");

		if (adminDomain != null && !adminDomain.trim().isEmpty() && adminEmail != null
				&& !adminEmail.trim().isEmpty()) {
			return StreamSupport.stream(this.actionDao.findAll().spliterator(), false).map(this.converter::fromEntity)
					.collect(Collectors.toList());
		} else {
			throw new RuntimeException("Admin Domain and Admin Email must not be empty or null");
		}

	}

	@Override
	@Transactional
	public void deleteAllActions(String adminDomain, String adminEmail) {

		UserIdEntity uIB = new UserIdEntity(adminDomain, adminEmail);

		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by adminDomain:" + adminDomain + "or adminEmail:" + adminEmail));

		if (!existingUser.getRole().equals(UserRole.ADMIN))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only Admin can delete actions!");

		if (adminDomain != null && !adminDomain.trim().isEmpty() && adminEmail != null
				&& !adminEmail.trim().isEmpty()) {
			this.actionDao.deleteAll();
		} else {
			throw new RuntimeException("Admin Domain and Admin Email must not be empty or null");
		}

	}

	@Override
	@Transactional(readOnly = true)
	public List<ActionBoundary> getAllActions(String adminDomain, String adminEmail, int size, int page) {
		UserIdEntity uIB = new UserIdEntity(adminDomain, adminEmail);

		UserEntity existingUser = this.userDao.findById(uIB).orElseThrow(() -> new UserNotFoundException(
				"could not find user by adminDomain:" + adminDomain + "or adminEmail:" + adminEmail));

		if (!existingUser.getRole().equals(UserRole.ADMIN))
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only Admin can export actions!");

		return this.actionDao.findAll(PageRequest.of(page, size, Direction.DESC, "actionId"))// Page<ActionEntity>
				.getContent()// List<ActionEntity>
				.stream()// Stream<ActionEntity>
				.map(this.converter::fromEntity)// Stream<ActionEntity>
				.collect(Collectors.toList()); // List<ActionEntity>

	}
	
	
	
	
	
	
	public void createClassRooms(ElementBoundary student, boolean leave) {

		
		HashMap<String, Object> currentClassAttributes = new HashMap<>();
		ElementBoundary classRoom;

		for(int i = 0; i < 10; i++) {
			
			currentClassAttributes.put("room Number", i);
			currentClassAttributes.put("status", "available");
			 classRoom = new ElementBoundary(new ElementIdBoundary(projectName,  UUID.randomUUID().toString()), "classRoom", "class number" + i,
					true, new Date(), student.getCreatedBy() ,new ElementLocationBoundary(3.44, 4.22) , currentClassAttributes);
			this.elementDao.save(this.converter.toEntity(classRoom));
		
		}

	}
	
		
	
	public ElementBoundary updateClassRoom(ElementBoundary student, boolean leaveClass, UserBoundary userBoundary) {
		
		ElementBoundary classBoundary = null;
		
		List<String> studentList = new ArrayList<>();
		int counter = 0, capacity = 0;
		
		counter = (int) classBoundary.getElementAttributes().get(ClassAttributes.studentCounter.name());
		
		studentList = (ArrayList<String>) classBoundary.getElementAttributes().get(ClassAttributes.studentList.name());
		
		capacity = (int) classBoundary.getElementAttributes().get(ClassAttributes.capacity.name());
		
		
		if(!leaveClass) {
		if(capacity < counter+1) 
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "the classRoom is full");
		
		
		studentList.add(student.getElementId().toString());
		classBoundary.getElementAttributes().put(ClassAttributes.studentCounter.name(), ++counter);
		}
		
		
		if(leaveClass) {
			studentList.remove(student.getElementId().toString());
			classBoundary.getElementAttributes().put(ClassAttributes.studentCounter.name(), --counter);
		}
		
		if(counter >= capacity)
			classBoundary.setActive(false);
		else
			classBoundary.setActive(true);
		
		classBoundary.getElementAttributes().put(ClassAttributes.StudentReportTime.name(), new Date().toString());
		
		
		classBoundary.getElementAttributes().put(ClassAttributes.studentList.name(), studentList.toArray(new String[0]));
		
		
		
		
		return this.elementService.update(userBoundary.getUserId().getDomain(),userBoundary.getUserId().getEmail(), classBoundary.getElementId().getDomain(),classBoundary.getElementId().getId(), classBoundary);
	
		
		
	}
		
	
	public ElementBoundary enterClassOrLeaveClass(ElementBoundary student, UserBoundary user, boolean leave,ActionBoundary action) {
		
		ElementBoundary classBoundary = null;
		double distanceFromClass = 0.0002;
		
		
		ElementBoundary[] studentsInClass = elementService.getAnArrayWithElementParent(user.getUserId().getDomain(), user.getUserId().getEmail(), student.getElementId().getDomain(), student.getElementId().getId(), 1, 0).toArray(new ElementBoundary[0]);
		
		if(studentsInClass.length > 0)
			if(studentsInClass[0].getType().equals("classRoom"))
				classBoundary = updateClassRoom(student, leave, user);
			else if(studentsInClass[0].getType().equals("college"))
		
		
		
		if(classBoundary == null) {
			ElementBoundary[] closestClass = this.elementService.searchByLocation(user.getUserId().getDomain(), user.getUserId().getEmail(), student.getLocation().getLat(), student.getLocation().getLng(), distanceFromClass*4, 30, 0).toArray(new ElementBoundary[0]);
			
			if(closestClass.length > 0 && classBoundary == null )
			classBoundary =  updateClassRoom(student, leave, user);
		
		}
		return classBoundary;
	}
	
}
