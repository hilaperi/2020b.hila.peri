package acs.aop;

import acs.aop.PerformanceUnit;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface PerformanceElapsedMonitor {
	PerformanceUnit units() default PerformanceUnit.ms;

}
