package acs.rest;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import acs.logic.ExtendedElementService;
import acs.rest.boundaries.element.ElementBoundary;
import acs.rest.boundaries.element.ElementIdBoundary;

@RestController
public class ElementController {

	private ExtendedElementService elementService;

	@Autowired
	public void setElementService(ExtendedElementService elementService) {
		this.elementService = elementService;
	}

	@RequestMapping(path = "/acs/elements/{managerDomain}/{managerEmail}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary createNewElement(@PathVariable("managerDomain") String managerDomain,
			@PathVariable("managerEmail") String managerEmail, @RequestBody ElementBoundary elementDetails)  {

		return this.elementService.create(managerDomain, managerEmail, elementDetails);
	}

	
	@RequestMapping(path = "/acs/elements/{managerDomain}/{managerEmail}/{elementDomain}/{elementId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)

	public void updateElement(@PathVariable("managerDomain") String managerDomain,
			@PathVariable("managerEmail") String managerEmail, @PathVariable("elementDomain") String elementDomain,
			@PathVariable("elementId") String elementId, @RequestBody ElementBoundary elemB)  {

		this.elementService.update(managerDomain, managerEmail, elementDomain, elementId, elemB);

	}

	// http GET method - returns element
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/{elementDomain}/{elementId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)

	public ElementBoundary retreiveElement(@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail, @PathVariable("elementDomain") String elementDomain,
			@PathVariable("elementId") String elementId)  {

		return this.elementService.getSpecificElement(userDomain, userEmail, elementDomain, elementId);
	}

//	 GET method - returns Array Of Elements
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] retreiveElementArr(@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {

		return this.elementService.getAll(userDomain, userEmail, size, page).toArray(new ElementBoundary[0]);
																									// ElementBoundry

	}
	
	
	@RequestMapping(path = "/acs/elements/{managerDomain}/{managerEmail}/{elementDomain}/{elementId}/children", method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE)
	public void bindExistingElementToAnExistingChildElement(@PathVariable("managerDomain") String managerDomain, @PathVariable("managerEmail") String managerEmail, @PathVariable("elementDomain") String elementDomain , @PathVariable("elementId") String elementParentId,@RequestBody ElementIdBoundary elementChildIdBoundary) {
		this.elementService.bindExistingElementToAnExistingChildElement(managerDomain,managerEmail,new ElementIdBoundary(elementDomain,elementParentId), elementChildIdBoundary);
		
	}
	
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/{elementDomain}/{elementId}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAllChildrenOfAnExistingElement(@PathVariable("userDomain") String userDomain, @PathVariable("userEmail") String userEmail, @PathVariable("elementDomain") String elementDomain, @PathVariable("elementId") String elementParentId,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		
		return this.elementService.getAllChildrenOfAnExsitingElement(userDomain, userEmail, elementDomain,
				String.valueOf(elementParentId), size, page).toArray(new ElementBoundary[0]);
	}
	
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/{elementDomain}/{elementId}/parents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getAnArrayWithElementParent(@PathVariable("userDomain") String userDomain, @PathVariable("userEmail") String userEmail, @PathVariable("elementDomain") String elementDomain, @PathVariable("elementId") String elementChildId,@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		
		if (size <= 0) {
			throw new RuntimeException("illegal size. size should be positive");
		}
		
		if (page < 0) {
			throw new RuntimeException("illegal page. page should be non negative");
		}
		
		
		
		return this.elementService.getAnArrayWithElementParent(userDomain, userEmail, elementDomain, elementChildId, size, page).toArray(new ElementBoundary[0]);
		
	}
	
	
	@RequestMapping(path = "/acs/elements/{UserDomain}/{UserEmail}/search/byName/{name}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] searchElemntsByName(
			@PathVariable("UserDomain") String userDomain,
			@PathVariable("UserEmail") String userEmail,
			@PathVariable("name") String name,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size, 
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService
		.getElementsByName(userDomain, userEmail, name, size, page)
		.stream()
		.collect(Collectors.toList())
		.toArray(new ElementBoundary[0]);
	}
	
	
	@RequestMapping(path = "/acs/elements/{userDomain}/{userEmail}/search/near/{lat}/{lng}/{distance}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] searchByLocation(@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail, @PathVariable("lat") double lat,
			@PathVariable("lng") double lng, @PathVariable("distance") double distance,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {

		return this.elementService.searchByLocation(userDomain, userEmail, lat, lng, distance, size, page)
				.toArray(new ElementBoundary[0]);
	}
	
	
	
	@RequestMapping(
			path="/acs/elements/{userDomain}/{userEmail}/search/byType/{type}", 
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary[] getElementsByType (@PathVariable("userDomain") String userDomain,
			@PathVariable("userEmail") String userEmail,
			@PathVariable("type") String type,
			@RequestParam(name = "size", required = false, defaultValue = "10") int size, 
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService
				.getElementsByType(userDomain, userEmail,type, size, page)
				.toArray(new ElementBoundary[0]);
	}
	
	
}
