package acs.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acs.logic.ActionService;
import acs.rest.boundaries.action.ActionBoundary;

@RestController
public class ActionController {

	private ActionService actionService;

	// inject of other Spring Bean
	@Autowired
	public void setActionService(ActionService actionService) {
		this.actionService = actionService;
	}

	@RequestMapping(path = "/acs/actions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object invokeAnAction(@RequestBody ActionBoundary actionDetails) {

		return this.actionService.invokeAction(actionDetails);

	}
}
