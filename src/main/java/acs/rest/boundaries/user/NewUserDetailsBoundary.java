package acs.rest.boundaries.user;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import acs.data.UserRole;

public class NewUserDetailsBoundary {

	private String email;
	private UserRole role;
	private String username;
	private String avatar;
	private Pattern regexPattern;
	private Matcher regMatcher;

	public NewUserDetailsBoundary() {

	}

	public NewUserDetailsBoundary(String email, UserRole role, String username, String avatar) {
		super();
		this.email = email;
		this.role = role;
		this.username = username;
		this.avatar = avatar;

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
		regMatcher = regexPattern.matcher(email);
		if (regMatcher.matches()) {
			this.email = email;
		} else {
			throw new RuntimeException("Invalid Email Address");
		}
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@Override
	public String toString() {
		return "NewUserDetailsBoundary [email=" + email + ", role=" + role + ", username=" + username + ", avatar="
				+ avatar + "]";
	}

}
