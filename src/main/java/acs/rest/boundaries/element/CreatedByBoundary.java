package acs.rest.boundaries.element;

import acs.rest.boundaries.user.UserIdBoundary;

public class CreatedByBoundary {

	private UserIdBoundary userId;
	
	
	public CreatedByBoundary() {
	}
	
	public CreatedByBoundary(UserIdBoundary userId) {
		this.userId = userId;
	}

	public UserIdBoundary getUserId() {
		return userId;
	}

	public void setUserId(UserIdBoundary userId) {
		this.userId = userId;
	}
}
