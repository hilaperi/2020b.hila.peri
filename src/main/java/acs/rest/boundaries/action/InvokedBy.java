package acs.rest.boundaries.action;

import acs.rest.boundaries.user.UserIdBoundary;

public class InvokedBy {

	private UserIdBoundary userId;
	
	public InvokedBy() {
	}
	
	public InvokedBy(UserIdBoundary userId) {
		this.userId = userId;
	}

	public UserIdBoundary getUserId() {
		return userId;
	}

	public void setUserId(UserIdBoundary userId) {
		this.userId = userId;
	}
	
}
