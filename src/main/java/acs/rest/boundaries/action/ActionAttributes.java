package acs.rest.boundaries.action;


public class ActionAttributes {


	private String key1;
	private Double key2;
	private Boolean booleanValue;
	private String lastKey;
	
	
	public ActionAttributes() {

	}


	public ActionAttributes(String key1, Double key2, boolean booleanValue, String lastKey) {
		super();
		this.key1 = key1;
		this.key2=key2;
		this.booleanValue=booleanValue;
		this.lastKey=lastKey;
		
	}

	
	public String getKey1() {
		return key1;
	}


	public void setKey1(String key1) {
		this.key1 = key1;
	}


	public Double getKey2() {
		return key2;
	}


	public void setKey2(Double key2) {
		this.key2 = key2;
	}


	public boolean isBooleanValue() {
		return booleanValue;
	}


	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}


	public String getLastKey() {
		return lastKey;
	}


	public void setLastKey(String lastKey) {
		this.lastKey = lastKey;
	}


	
	
	


	

	



	
}


