package acs.rest.boundaries.action;

import acs.rest.boundaries.element.ElementIdBoundary;

public class ElementOfAction {

	private ElementIdBoundary elementId;
	
	
	public ElementOfAction() {

	}
	
	public ElementOfAction(ElementIdBoundary elementId) {
		this.elementId = elementId;
	}

	public ElementIdBoundary getElementId() {
		return elementId;
	}

	public void setElementIdBoundary(ElementIdBoundary elementId) {
		this.elementId = elementId;
	}
}
