package acs.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acs.logic.UserService;
import acs.rest.boundaries.user.NewUserDetailsBoundary;
import acs.rest.boundaries.user.UserBoundary;
import acs.rest.boundaries.user.UserIdBoundary;

@RestController
public class UserController {
	private UserService userService;

	@Autowired
	private void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(path = "/acs/users", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)

	public UserBoundary createNewUser(@RequestBody NewUserDetailsBoundary newUserDetails) {
		UserBoundary ub = new UserBoundary();  
		ub.setUserId(new UserIdBoundary("", newUserDetails.getEmail()));	
		ub.setRole(newUserDetails.getRole());
		ub.setUsername(newUserDetails.getUsername());
		ub.setAvatar(newUserDetails.getAvatar());
		return this.userService.createUser(ub);
		
		
	}
	
	
	

	// check if its ok, when is the case that the name is not valid ?
	@RequestMapping(path = "/acs/users/login/{userDomain}/{userEmail}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)

	public UserBoundary loginValidUser(@PathVariable("userDomain") String domain,
			@PathVariable("userEmail") String email) {
		return this.userService.login(domain, email);

	}

	@RequestMapping(path = "/acs/users/{userDomain}/{userEmail}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)

	public void updateUserDetails(@PathVariable("userDomain") String domain, @PathVariable("userEmail") String email,
			@RequestBody UserBoundary UB) {
		this.userService.updateUser(domain, email, UB);

	}
	


}