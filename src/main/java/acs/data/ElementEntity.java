package acs.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import acs.dal.MapToJsonConverter;

@Embeddable
@Entity
@Table(name="ELEMENTS")
public class ElementEntity {

	private ElementIdEntity elementId;
	private String type;
	private String name;
	private boolean active;
	private Date createdTimestamp;
	private String createdBy;
	private ElementLocationEntity location;
	private Map<String, Object> elementAttributes;
	private ElementEntity parent;
	private Set<ElementEntity> children;
	
	
	public ElementEntity() {
		this.children = new HashSet<>();

	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	@EmbeddedId
	public ElementIdEntity getElementId() {
		return elementId;
	}

	public void setElementId(ElementIdEntity elementId) {
		this.elementId = elementId;
	}

	@Column(nullable = false)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Column(nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}


	@Embedded
	public ElementLocationEntity getLocation() {
		return location;
	}

	public void setLocation(ElementLocationEntity location) {
		this.location = location;
	}

	@Convert(converter = MapToJsonConverter.class)
	@Lob
	public Map<String, Object> getElementAttributes() {
		return elementAttributes;
	}

	public void setElementAttributes(Map<String, Object> elementAttributes) {
		this.elementAttributes = elementAttributes;
	}
	@Embedded
	@ManyToOne(fetch = FetchType.LAZY)
	public ElementEntity getParent() {
		return parent;
	}

	public void setParent(ElementEntity parent) {
		this.parent = parent;
	}

	@OneToMany(mappedBy = "parent",fetch = FetchType.LAZY)
	public Set<ElementEntity> getChildren() {
		return children;
	}

	public void setChildren(Set<ElementEntity> children) {
		this.children = children;
	}
	
	
	public void addChild (ElementEntity child) {
		this.children.add(child);
		child.setParent(this);
	}

}
