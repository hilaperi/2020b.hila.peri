package acs.data;

import org.springframework.stereotype.Component;

import acs.rest.boundaries.action.ActionBoundary;
import acs.rest.boundaries.action.ActionIdBoundary;
import acs.rest.boundaries.action.ElementOfAction;
import acs.rest.boundaries.action.InvokedBy;
import acs.rest.boundaries.element.CreatedByBoundary;
import acs.rest.boundaries.element.ElementBoundary;
import acs.rest.boundaries.element.ElementIdBoundary;
import acs.rest.boundaries.element.ElementLocationBoundary;
import acs.rest.boundaries.user.UserBoundary;
import acs.rest.boundaries.user.UserIdBoundary;

@Component
public class Converter {

	public UserIdBoundary fromEntity(UserIdEntity entity) {
		UserIdBoundary boundary = new UserIdBoundary();
		boundary.setDomain(entity.getDomain());
		boundary.setEmail(entity.getEmail());

		return boundary;

	}

	public UserBoundary fromEntity(UserEntity entity) {
		UserBoundary boundary = new UserBoundary();
		UserIdBoundary userId = fromEntity(entity.getUserId());
		if (entity.getUserId() != null) {
			boundary.setUserId(userId);
		}

		if (entity.getRole() != null) {
			boundary.setRole(entity.getRole());
		} else {
			boundary.setRole(UserRole.PLAYER);// or null ?
		}

		boundary.setUsername(entity.getUsername());
		boundary.setAvatar(entity.getAvatar());

		return boundary;

	}

	public UserIdEntity toEntity(UserIdBoundary boundary) {
		UserIdEntity entity = new UserIdEntity();
		entity.setDomain(boundary.getDomain());
		entity.setEmail(boundary.getEmail());

		return entity;
	}

	public UserEntity toEntity(UserBoundary boundary) {
		UserEntity entity = new UserEntity();
		UserIdEntity userId = toEntity(boundary.getUserId());
		if (boundary.getUserId() != null) {
			entity.setUserId(userId);
		} else {
			entity.setUserId(null);
		}
		if (boundary.getRole() != null) {
			entity.setRole(boundary.getRole());
		} else {
			entity.setRole(UserRole.PLAYER);// or null ?
		}

		entity.setUsername(boundary.getUsername());
		entity.setAvatar(boundary.getAvatar());

		return entity;

	}

	// -------------------------------------------------------

	public ElementIdBoundary fromEntity(ElementIdEntity entity) {
		ElementIdBoundary eIB = new ElementIdBoundary();
		eIB.setDomain(entity.getDomain());
		eIB.setId(entity.getId());

		return eIB;

	}

	public ElementLocationBoundary fromEntity(ElementLocationEntity entity) {
		ElementLocationBoundary eLB = new ElementLocationBoundary();
		
		eLB.setLat(entity.getLat());
		eLB.setLng(entity.getLng());
		
		return eLB;
		
	}
	
	public ElementLocationEntity toEntity(ElementLocationBoundary boundary) {
		ElementLocationEntity eLE = new ElementLocationEntity();
		eLE.setLat(boundary.getLat());
		eLE.setLng(boundary.getLng());
		
		return eLE;
	}
	
	public ElementBoundary fromEntity(ElementEntity entity) {
		ElementIdBoundary eIB = fromEntity(entity.getElementId());
		ElementLocationBoundary location = fromEntity(entity.getLocation());
		ElementBoundary elementBoundary = new ElementBoundary();
		UserIdBoundary userId = new UserIdBoundary();
		if (entity.getElementId() != null) {
			elementBoundary.setElementId(eIB);
		} else {
			elementBoundary.setElementId(null);
			;

		}

		elementBoundary.setType(entity.getType());
		elementBoundary.setName(entity.getName());
		elementBoundary.setActive(entity.getActive());
		elementBoundary.setCreatedTimestamp(entity.getCreatedTimestamp());

		if (entity.getCreatedBy() != null) {
			String[] idParts = entity.getCreatedBy().split(" ");
			userId.setDomain(idParts[0]);
			userId.setEmail(idParts[1]);
			elementBoundary.setCreatedBy(new CreatedByBoundary(userId));

		} else {
			elementBoundary.setCreatedBy(null);
		}

		if (entity.getLocation() != null) {
			elementBoundary.setLocation(location);
		} else {
			elementBoundary.setLocation(null);
		}

		elementBoundary.setElementAttributes(entity.getElementAttributes());

		return elementBoundary;
	}

	public ElementIdEntity toEntity(ElementIdBoundary boundary) {

		ElementIdEntity eIE = new ElementIdEntity();
		eIE.setDomain(boundary.getDomain());
		eIE.setId(boundary.getId());

		return eIE;

	}

	public ElementEntity toEntity(ElementBoundary boundary) {
		ElementEntity entity = new ElementEntity();
		ElementIdEntity eIE = toEntity(boundary.getElementId());
		ElementLocationEntity eLE = toEntity(boundary.getLocation());
		if (boundary.getElementId() != null) {
			entity.setElementId(eIE);
		} else {
			entity.setElementId(null);
		}

		entity.setType(boundary.getType());
		entity.setName(boundary.getName());

		if (boundary.getActive() != null) {
			entity.setActive(boundary.getActive());

		} else {
			entity.setActive(false);

		}

		entity.setCreatedTimestamp(boundary.getCreatedTimestamp());

		if (boundary.getCreatedBy() != null) {
			entity.setCreatedBy(boundary.getCreatedBy().getUserId().getDomain() + " "
					+ boundary.getCreatedBy().getUserId().getEmail());

		} else {
			entity.setCreatedBy(" ");
		}

		if (boundary.getLocation() != null) {
			entity.setLocation(eLE);
		} else {
			entity.setLocation(null);
		}

		if (boundary.getElementAttributes() != null) {
			entity.setElementAttributes(boundary.getElementAttributes());

		} else {
			entity.setElementAttributes(null);
		}

		return entity;

	}

	// ----------------------------------------------------------

	
	
	
	public ActionBoundary fromEntity(ActionEntity entity) {
		ActionIdBoundary actionIdBoundary;
		UserIdBoundary userId = new UserIdBoundary();
		ActionBoundary ab = new ActionBoundary();
		ElementIdBoundary eb = fromEntity(entity.getElement());
		if (entity.getActionId() != null) {
			String[] idParts = entity.getActionId().split(" ");
			actionIdBoundary = new ActionIdBoundary();
			actionIdBoundary.setDomain(idParts[0]);
			actionIdBoundary.setId(idParts[1]);
			ab.setActionId(actionIdBoundary);
		} else {
			actionIdBoundary = null;
		}
		ab.setType(entity.getType());
		if (entity.getElement() != null) {
			ab.setElement(new ElementOfAction(eb));
		} else {
			ab.setElement(null);
		}
		ab.setCreatedTimestamp(entity.getCreatedTimestamp());
		if (entity.getInvokedBy() != null) {
			String[] idParts = entity.getInvokedBy().split(" ");
			userId.setDomain(idParts[0]);
			userId.setEmail(idParts[1]);
			ab.setInvokedBy(new InvokedBy(userId));

		} else {
			ab.setInvokedBy(null);
		}
		ab.setActionAttributes(entity.getActionAttributes());

		return ab;

	}

	public ActionEntity toEntity(ActionBoundary boundary) {
		ElementIdEntity eIB = toEntity(boundary.getElement().getElementId());
		ActionEntity entity = new ActionEntity();
		if (boundary.getActionId() != null) {
			entity.setActionId(boundary.getActionId().getDomain() + " " + boundary.getActionId().getId());
		} else {
			entity.setActionId(" ");
		}

		if (boundary.getElement() != null) {
			entity.setElement(eIB);
		} else {
			entity.setElement(null);
		}
		entity.setType(boundary.getType());
		entity.setCreatedTimestamp(boundary.getCreatedTimestamp());
		if (boundary.getInvokedBy() != null) {
			entity.setInvokedBy(boundary.getInvokedBy().getUserId().getDomain() + " "
					+ boundary.getInvokedBy().getUserId().getEmail());
		} else {
			entity.setInvokedBy(" ");
		}
		entity.setActionAttributes(boundary.getActionAttributes());

		return entity;

	}
}
